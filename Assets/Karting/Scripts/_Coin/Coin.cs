﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour, IPickable
{
    public delegate void AddCoinInCurrentRace();
    public static event AddCoinInCurrentRace OnAddCoinInCurrentRace;
    
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            if (OnAddCoinInCurrentRace != null) OnAddCoinInCurrentRace();

            DoDestroy();
        }
    }
    
    public void DoDestroy()
    {
        Destroy(gameObject);
    }
}

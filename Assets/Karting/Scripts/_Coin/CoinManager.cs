﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace KartGame.Track
{
    public class CoinManager : MonoBehaviour
    {
        private int m_coinsInCurrentRace;
        private int m_coinsInTotal;

        private void Awake()
        {
            m_coinsInCurrentRace = 0;
            m_coinsInTotal = PlayerPrefs.GetInt("TotalCoins");
            Coin.OnAddCoinInCurrentRace += HandleAddCoinInCurrentRace;
            TrackManager.OnSetTotalCoins += HandleSetCoinsInTotal;
        }

        void HandleAddCoinInCurrentRace()
        {
            m_coinsInCurrentRace++;
        }

        public void HandleSetCoinsInTotal()
        {
            m_coinsInTotal += m_coinsInCurrentRace;
            PlayerPrefs.SetInt("TotalCoins", m_coinsInTotal);
        }

        private void OnDestroy()
        {
            Coin.OnAddCoinInCurrentRace -= HandleAddCoinInCurrentRace;
            TrackManager.OnSetTotalCoins -= HandleSetCoinsInTotal;
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace KartGame.Track
{
    public class CoinsUI : MonoBehaviour
    {
        private TextMeshProUGUI textComponent;

        private int m_coinsInCurrentRace;

        // Start is called before the first frame update
        void Start()
        {
            textComponent = GetComponentInChildren<TextMeshProUGUI>();
            Coin.OnAddCoinInCurrentRace += HandleAddCoinInCurrentRace;
            m_coinsInCurrentRace = 0;
        }

        public void HandleAddCoinInCurrentRace()
        {
            m_coinsInCurrentRace++;
            SetCoinsText();
        }

        void SetCoinsText()
        {
            textComponent.text = "Coins: " + m_coinsInCurrentRace.ToString();
        }

        private void OnDestroy()
        {
            Coin.OnAddCoinInCurrentRace -= HandleAddCoinInCurrentRace;
        }
    }
}

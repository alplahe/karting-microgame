﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace KartGame.Track
{
    public class TotalCoinsMainMenuUI : MonoBehaviour
    {
        private TextMeshProUGUI textComponent;
        
        // Start is called before the first frame update
        void Start()
        {
            textComponent = GetComponentInChildren<TextMeshProUGUI>();

            SetCoinsText();
        }
        
        void SetCoinsText()
        {
            textComponent.text = "Total coins: " + PlayerPrefs.GetInt("TotalCoins").ToString();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class BestTimeUI : MonoBehaviour
{
    TextMeshProUGUI m_bestTime;
    
    private void OnEnable()
    {
        m_bestTime = GetComponent<TextMeshProUGUI>();
        m_bestTime.text = "Best Time: " + PlayerPrefs.GetFloat("BestTime").ToString();
    }
}
